# gitlab-runner-garbage-collector

Clean old gitlab-runner build files by cron 

##  ! assumes the following things:

* username for gitlab-runner is : gitlab-runner ( end of su command)
* the user who runs this may su into gitlab runner and has doccker socket access
* deletes files older than 14 days in build dir (MTIME)
* all stopped/unused containers/networks/volumes on this machine may be deleted as the script runs `docker system prune -a --force` 
* cleans systemd journal ( keeps only 7 days)

### crontab example 
`23 5    * * *   /bin/bash /etc/custom/gitlab-runner-garbage-collector/cron-run.sh `

### crontab example to log into a file, sending no mail
`23 5    * * *   /bin/bash /etc/custom/gitlab-runner-garbage-collector/cron-run.sh &>/tmp/gitlab-garbage-collect.log`
---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/gitlab-runner-garbage-collector/README.md/logo.jpg" width="480" height="270"/></div></a>
