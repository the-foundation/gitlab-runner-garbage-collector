#!/bin/bash
su -s /bin/bash -c 'CTIME=14; cd ;test -d builds && ( deleted=$(find builds -mindepth 4 -ctime +${CTIME} -print |while read a ;do rm -rf "$a" ;echo "$a";done ) ;echo -e "\e[1;31m deleted "$(echo "$deleted"|grep -v ^$|wc -l)" Items\e[0m" ; find builds -mindepth 4  -type d -ctime +${CTIME}  -delete)' gitlab-runner
which docker && ( ( docker image prune -a --filter "until=24h" --force 2>&1 ;  docker container prune --filter "until=24h" --force  2>&1 ;  docker network prune --filter "until=24h" --force  2>&1 ) &>/tmp/gitlab-prune.log )
which journalctl && journalctl --vacuum-time=7d